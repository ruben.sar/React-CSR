/**
 * Created by User on 11.10.2018.
 */


const express = require('express');
const api = express.Router();
const data = require('../_mockData');


const router = function () {

  api.route('/list').get(function (req, res) {

    res.status(200).send({
      customers: data.customers,
      success: true
    });

  });

  api.route('/:id').get(function (req, res) {

    const customer = data.customers.filter(customer=> {
      return customer.id == req.params.id;
    })[0];

    if (customer) {
      res.status(200).send({
        customer: customer,
        success: true
      });
    } else {
      res.status(200).send({
        customer: null,
        success: false
      });
    }

  });

  return api;
};

module.exports = router();
