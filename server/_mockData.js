/**
 * Created by Ruben on 11.10.2018.
 */

const customers = [{
  "id": 1,
  "firstName": "Min",
  "lastName": "Sallan",
  "email": "msallan0@stumbleupon.com",
  "gender": "Female",
  "avatar": "https://robohash.org/laborumquidemrepellat.jpg?size=80x80&set=set1"
},
  {
    "id": 2,
    "firstName": "Kenyon",
    "lastName": "Grainger",
    "email": "kgrainger1@mozilla.org",
    "gender": "Male",
    "avatar": "https://robohash.org/verodolormaiores.jpg?size=80x80&set=set1"
  },
  {
    "id": 3,
    "firstName": "Nady",
    "lastName": "Keers",
    "email": "nkeers2@etsy.com",
    "gender": "Female",
    "avatar": "https://robohash.org/autquisdoloremque.jpg?size=80x80&set=set1"
  },
  {
    "id": 4,
    "firstName": "Luis",
    "lastName": "Osmon",
    "email": "losmon3@walmart.com",
    "gender": "Male",
    "avatar": "https://robohash.org/porroipsavoluptatem.jpg?size=80x80&set=set1"
  },
  {
    "id": 5,
    "firstName": "Helyn",
    "lastName": "Attew",
    "email": "hattew4@flickr.com",
    "gender": "Female",
    "avatar": "https://robohash.org/magnicorporisdolor.jpg?size=80x80&set=set1"
  },
  {
    "id": 6,
    "firstName": "Jeremie",
    "lastName": "Benettini",
    "email": "jbenettini5@odnoklassniki.ru",
    "gender": "Male",
    "avatar": "https://robohash.org/sedsuntdolorem.jpg?size=80x80&set=set1"
  },
  {
    "id": 7,
    "firstName": "Welch",
    "lastName": "Bretland",
    "email": "wbretland6@redcross.org",
    "gender": "Male",
    "avatar": "https://robohash.org/consequaturcorruptiipsum.jpg?size=80x80&set=set1"
  },
  {
    "id": 8,
    "firstName": "Lonnie",
    "lastName": "O' Hern",
    "email": "lohern7@ox.ac.uk",
    "gender": "Female",
    "avatar": "https://robohash.org/eosnatussapiente.jpg?size=80x80&set=set1"
  },
  {
    "id": 9,
    "firstName": "Cale",
    "lastName": "McCadden",
    "email": "cmccadden8@statcounter.com",
    "gender": "Male",
    "avatar": "https://robohash.org/esseautsequi.jpg?size=80x80&set=set1"
  },
  {
    "id": 10,
    "firstName": "Worden",
    "lastName": "Maybury",
    "email": "wmaybury9@tiny.cc",
    "gender": "Male",
    "avatar": "https://robohash.org/hicnulladucimus.jpg?size=80x80&set=set1"
  }];

const partners = [{
  "id": 1,
  "firstName": "Tomkin",
  "lastName": "Issacov",
  "email": "tissacov0@odnoklassniki.ru",
  "gender": "Male",
  "avatar": "https://robohash.org/porrodoloresaccusantium.jpg?size=50x50&set=set1"
},
  {
    "id": 2,
    "firstName": "Land",
    "lastName": "Demare",
    "email": "ldemare1@senate.gov",
    "gender": "Male",
    "avatar": "https://robohash.org/oditminusatque.jpg?size=50x50&set=set1"
  },
  {
    "id": 3,
    "firstName": "Antonius",
    "lastName": "Braundt",
    "email": "abraundt2@yelp.com",
    "gender": "Male",
    "avatar": "https://robohash.org/iustoutnon.bmp?size=50x50&set=set1"
  },
  {
    "id": 4,
    "firstName": "Kristine",
    "lastName": "Tassel",
    "email": "ktassel3@amazon.com",
    "gender": "Female",
    "avatar": "https://robohash.org/quosnatusqui.bmp?size=50x50&set=set1"
  },
  {
    "id": 5,
    "firstName": "Sascha",
    "lastName": "Lambertini",
    "email": "slambertini4@fotki.com",
    "gender": "Male",
    "avatar": "https://robohash.org/etquaeratsed.bmp?size=50x50&set=set1"
  },
  {
    "id": 6,
    "firstName": "Barbra",
    "lastName": "Kennaway",
    "email": "bkennaway5@prlog.org",
    "gender": "Female",
    "avatar": "https://robohash.org/dolorreprehenderitet.png?size=50x50&set=set1"
  },
  {
    "id": 7,
    "firstName": "Willy",
    "lastName": "Burtt",
    "email": "wburtt6@webmd.com",
    "gender": "Female",
    "avatar": "https://robohash.org/autemsintfacilis.jpg?size=50x50&set=set1"
  },
  {
    "id": 8,
    "firstName": "Marguerite",
    "lastName": "Dawby",
    "email": "mdawby7@tuttocitta.it",
    "gender": "Female",
    "avatar": "https://robohash.org/asperioresrerumtemporibus.jpg?size=50x50&set=set1"
  },
  {
    "id": 9,
    "firstName": "Gearard",
    "lastName": "Kewley",
    "email": "gkewley8@fotki.com",
    "gender": "Male",
    "avatar": "https://robohash.org/dictaeligendimolestiae.png?size=50x50&set=set1"
  },
  {
    "id": 10,
    "firstName": "Ethyl",
    "lastName": "MacMenamy",
    "email": "emacmenamy9@bloglines.com",
    "gender": "Female",
    "avatar": "https://robohash.org/sapientecorruptipariatur.png?size=50x50&set=set1"
  }];

module.exports = {
  partners: partners,
  customers: customers
};
