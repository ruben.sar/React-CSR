/**
 * Created by ruben on 3/4/2018.
 */

const express = require('express');
const webpack = require('webpack');
const path = require('path');
const config = require('./webpack.config.dev');
//react
const ReactDOMServer = require('react-dom/server');
const React = require('react');
//rout
import { StaticRouter , Route } from 'react-router-dom';
//redux
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import rootReducer from './public/_reducers/index';
import thunk from 'redux-thunk';
//app
import App from './public/components/App';
//API
const customersAPI = require('./server/api/customersApi');
const partnersAPI = require('./server/api/partnersApi');

const port = process.env.PORT || 3000;
const app = express();


app.use('/api/customers', customersAPI);
app.use('/api/partners', partnersAPI);
const compiler = webpack(config);
app.use(express.static('public'));
app.use(express.static('assets'));

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));


app.get('*', (req, res) => {

  const context = {};
  const preloadedState = {};

  const store = createStore(
    rootReducer,
    {},
    applyMiddleware(thunk)
  );

  const html = ReactDOMServer.renderToString(
    <Provider store={store}>
      <StaticRouter location={req.url} context={context}>
        <App />
      </StaticRouter>
    </Provider>
  );

  const finalState = store.getState();

  res.send(`
    <!doctype html>
    <html lang="en">
      <head>
         <title>${preloadedState.title}</title>
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
         <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
         <link rel="stylesheet" href="/css/app.css" />
      </head>
      <body>
           <div id="root">${html}</div>
           <script>
            // WARNING: See the following for security issues around embedding JSON in HTML:
            // http://redux.js.org/docs/recipes/ServerRendering.html#security-considerations
            window.__PRELOADED_STATE__ = ${JSON.stringify(finalState).replace(/</g, '\\u003c')}
          </script>
          <script src="/bundle.js"></script>
      </body>
   </html>
   `);
});

app.listen(port, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log(`running on http://localhost:${port} ...`);
  }
});
