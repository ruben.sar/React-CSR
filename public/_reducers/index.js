/**
 * Created by ruben on 3/11/2018.
 */

import {combineReducers} from 'redux';
import {customersListReducer, customerReducer} from './customerReducer';
import {partnersListReducer, partnerReducer} from './partnerReducer';


const rootReducer = combineReducers({
  customersList: customersListReducer,
  customer: customerReducer,
  partnersList: partnersListReducer,
  partner: partnerReducer
});

export default rootReducer;
