/**
 * Created by ruben on 11/10/2018.
 */

import {CUSTOMER}  from '../_actions/_actionTypes';
import initialState from './initialState';

export function customersListReducer(state = initialState.customersList, action) {
  switch (action.type) {
    case CUSTOMER.LIST_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}

export function customerReducer(state = initialState.customer, action) {
  switch (action.type) {
    case CUSTOMER.SINGLE_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}
