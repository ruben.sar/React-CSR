/**
 * Created by ruben on 12/10/2018.
 */
import {PARTNER}  from '../_actions/_actionTypes';
import initialState from './initialState';

export function partnersListReducer(state = initialState.partnersList, action) {
  switch (action.type) {
    case PARTNER.LIST_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}

export function partnerReducer(state = initialState.partner, action) {
  switch (action.type) {
    case PARTNER.SINGLE_LOAD_SUCCESS:
      return action.payload;
    default :
      return state;
  }
}
