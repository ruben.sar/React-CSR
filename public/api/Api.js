/**
 * Created by ruben on 3/11/2018.
 */
import axios from 'axios';


class Api {

  static getCustomers() {
    return axios.get('/api/customers/list');
  }

  static getCustomerById(id) {
    return axios.get(`/api/customers/${id}`);
  }

  static getPartners() {
    return axios.get('/api/partners/list');
  }

  static getPartnerById(id) {
    return axios.get(`/api/partners/${id}`);
  }
}

export default Api;
