/**
 * Created by ruben on 3/4/2018.
 */


import React from 'react';
import { hydrate } from 'react-dom';
import { BrowserRouter , Route } from 'react-router-dom'
import App from './components/App';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore.dev.js';


const preloadedState = window.__PRELOADED_STATE__;

delete window.__PRELOADED_STATE__;

const store = configureStore(preloadedState);


hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);
