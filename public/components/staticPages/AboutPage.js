/**
 * Created by Ruben on 3/4/2018.
 */

import React from 'react';
import Head from '../common/Head';


const AboutPage = ()=> {
  return (
    <div>
      <Head title={'About Us'} description={'Get more info about Us'}/>

      <div className="jumbotron text-center">
        <i className="fab fa-5x text-green fa-react"></i>

        <h1>About Page</h1>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-sm-4">
            <div className="text-center">
              <i className="fas fa-2x fa-file-contract"></i>

              <h3>Column 1 </h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
          <div className="col-sm-4">
            <div className="text-center">
              <i className="fas fa-2x fa-binoculars"></i>

              <h3>Column 2 </h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
          <div className="col-sm-4">
            <div className="text-center">
              <i className="fas fa-2x fa-file-signature"></i>

              <h3>Column 3</h3>

            </div>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
        </div>
      </div>
    </div>
  );


};

export default AboutPage;
