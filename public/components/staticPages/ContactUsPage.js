/**
 * Created by Ruben on 10.10.2018.
 */

import React from 'react';
import Head from '../common/Head';

const ContactUsPage = ()=> {

  return (
    <div>
      <Head title={'Contact Us'} description={'Get in touch with Us'}/>

      <div className="jumbotron text-center">
        <i className="fab fa-5x text-red fa-react"></i>

        <h1>Contact Us Page</h1>

      </div>

      <div className="container">
        <div className="row">
          <div className="col-sm-4">
            <div className="text-center">
              <i class="fas fa-2x fa-cogs"></i>

              <h3>Column 1</h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
          <div className="col-sm-4">
            <div className="text-center">
              <i class="fab fa-2x fa-cc-amazon-pay"></i>

              <h3>Column 2 </h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
          <div className="col-sm-4">
            <div className="text-center">
              <i class="fab fa-2x fa-btc"></i>

              <h3>Column 3</h3>
            </div>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit...</p>

            <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris...</p>
          </div>
        </div>
      </div>
    </div>
  );


};

export default ContactUsPage;
