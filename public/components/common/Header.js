/**
 * Created by ruben on 3/4/2018.
 */
import React from 'react';
import { NavLink  } from 'react-router-dom'
const Header = ()=> {
  return (
    <nav className="navbar navbar-default">

      <div className="container">
        <div className="navbar-header">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                  data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <NavLink className="navbar-brand" to="/">
            <i className="fab fa-react"></i> CSR
          </NavLink>
        </div>
        <div id="navbar" className="navbar-collapse collapse">
          <ul className="nav navbar-nav navbar-right">
            <li>
              <NavLink to="/" className="active">Home</NavLink>
            </li>
            <li>
              <NavLink  to="/about" className="active">About</NavLink>
            </li>
            <li>
              <NavLink  to="/contact-us" className="active">Contact Us</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default Header;
