/**
 * Created by Ruben on 12.10.2018.
 */

import React from 'react';
import {Link} from 'react-router-dom';

const PersonCard = ({person, type})=> {
  type = type || 'customer';
  const gender = ()=> {
    if (person.gender === 'Male') {
      return <i className="fas text-blue fa-2x fa-male"></i>
    } else {
      return <i className="fas fa-2x text-red fa-female"></i>
    }
  };

  return (
    <div className="person-card as-table">

      <div className="t-cell w-20">
        <img src={person.avatar} className="img-responsive center-block"/>
      </div>
      <div className="t-cell">
        <h3>{person.firstName + ' ' + person.lastName}</h3>

        <h4><i class="fas text-green fa-envelope"></i> {person.email}</h4>
      </div>

      <div className="t-cell w-10" align="center">
        {gender()}
      </div>

      <div className="t-cell w-20" align="center">
        <Link className="btn btn-primary" to={ `/${type}/${person.id}`}>View</Link>
      </div>

    </div>
  )
};
export default PersonCard;
