/**
 * Created by Ruben on 15.10.2018.
 */

import React from 'react';
import { NavLink  } from 'react-router-dom'
import {Helmet} from 'react-helmet';
const Head = ({title, description, img})=> {
  img = img || '/img/react.png';
  return (
    <Helmet>
      <meta charSet="utf-8"/>
      <title>{`${title} | React CSR`}</title>
      <meta property="og:title" content={`${title} | React CSR`}/>
      <meta property="og:description" content={description}/>
      <meta property="og:url" content={location.href}/>
      <meta property="og:image" content={img}/>
    </Helmet>
  );
};

export default Head;
