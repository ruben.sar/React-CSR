/**
 * Created by Ruben on 12.10.2018.
 */


import React from 'react';
import {connect} from 'react-redux';
import {fetchSinglePartner} from '../../_actions/partnersActions.js';
import Head from '../common/Head';


class PartnerPage extends React.Component {
  constructor(props) {
    super(props);

    this.checkForLog = this.checkForLog.bind(this);
  }


  componentDidMount() {
    this.props.fetchSinglePartner(this.props.match.params.id);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.props.fetchSinglePartner(this.props.match.params.id);
    }
  }


  checkForLog() {
    console.log('Home page correctly hydrated');
  }

  render() {
    const {partner} =this.props;

    return (
      <div>
        <Head title={`${partner.firstName} ${partner.lastName} (Partner)`} description={'React App with Client-Side Rendering'}/>
        <div className="jumbotron text-center">
          <img src={partner.avatar} className="center-block"/>

          <h1>{partner.firstName + ' ' + partner.lastName} </h1>

          <h3><i class="fas text-red fa-envelope"></i> {partner.email}</h3>
        </div>

      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    partner: state.partner
  };
}

const mapDispatchToProps = {
  fetchSinglePartner
};

export default connect(mapStateToProps, mapDispatchToProps)(PartnerPage);
