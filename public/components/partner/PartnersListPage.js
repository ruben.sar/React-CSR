/**
 * Created by Ruben on 12.10.2018.
 */

import React from 'react';
import {connect} from 'react-redux';
import {fetchPartners} from '../../_actions/partnersActions';
import PersonCard from '../common/PersonCard';
import Head from '../common/Head';


class CustomersListPage extends React.Component {
  constructor(props) {
    super(props);

    this.checkForLog = this.checkForLog.bind(this);
  }


  componentDidMount() {
    this.props.fetchPartners();
  }


  checkForLog() {
    console.log('Home page correctly hydrated');
  }

  render() {
    const {partnersList} =this.props;

    return (
      <div>
        <Head title={'Our Partners'} description={'React App with Client-Side Rendering'}/>
        <div className="jumbotron text-center">
          <i className="fas fa-5x text-green fa-handshake"></i>

          <h1>Our Partners</h1>
        </div>
        {partnersList.map(partner=> {
          return <PersonCard key={partner.id} person={partner} type={'partner'}/>
        })
        }

      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    partnersList: state.partnersList
  };
}

const mapDispatchToProps = {
  fetchPartners
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomersListPage);
