/**
 * Created by Ruben on 12.10.2018.
 */
/**
 * Created by ruben on 11.10.2018.
 */

import React from 'react';
import {connect} from 'react-redux';
import {fetchSingleCustomer} from '../../_actions/customersActions';
import Head from '../common/Head';


class CustomerPage extends React.Component {
  constructor(props) {
    super(props);

    this.checkForLog = this.checkForLog.bind(this);
  }


  componentDidMount() {
    this.props.fetchSingleCustomer(this.props.match.params.id);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.id !== this.props.match.params.id) {
      this.props.fetchSingleCustomer(this.props.match.params.id);
    }
  }


  checkForLog() {
    console.log('Home page correctly hydrated');
  }

  render() {
    const {customer} =this.props;

    return (
      <div>
        <Head title={`${customer.firstName} ${customer.lastName} (Customer)`}
              img={customer.avatar}
              description={'React App with Client-Side Rendering'}/>
        <div className="jumbotron text-center">
          <img src={customer.avatar} className="center-block"/>

          <h1>{customer.firstName + ' ' + customer.lastName} </h1>

          <h3><i class="fas text-red fa-envelope"></i> {customer.email}</h3>
        </div>

      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    customer: state.customer
  };
}

const mapDispatchToProps = {
  fetchSingleCustomer
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerPage);
