/**
 * Created by ruben on 11.10.2018.
 */

import React from 'react';
import {connect} from 'react-redux';
import {fetchCustomers} from '../../_actions/customersActions';
import PersonCard from '../common/PersonCard';
import Head from '../common/Head';


class CustomersListPage extends React.Component {
  constructor(props) {
    super(props);

    this.checkForLog = this.checkForLog.bind(this);
  }


  componentDidMount() {
    this.props.fetchCustomers();
  }


  checkForLog() {
    console.log('Home page correctly hydrated');
  }

  render() {
    const {customersList} =this.props;

    return (
      <div>
        <Head title={'Our Customers'} description={'React App with Client-Side Rendering'}/>
        <div className="jumbotron text-center">
          <i className="fa fa-5x text-green fa-users"></i>

          <h1>Our Customers</h1>
        </div>
        {customersList.map(customer=> {
          return <PersonCard key={customer.id} person={customer} type={'customer'}/>
        })
        }

      </div>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    customersList: state.customersList
  };
}

const mapDispatchToProps= {
    fetchCustomers
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomersListPage);
