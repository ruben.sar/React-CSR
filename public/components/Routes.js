/**
 * Created by ruben on 3/4/2018.
 */
import React from 'react';
import { Switch, Route } from 'react-router-dom'
import HomePage from './staticPages/HomePage';
import AboutPage from './staticPages/AboutPage';
import ContactUsPage from './staticPages/ContactUsPage';
import CustomersListPage from './customer/CustomersListPage';
import CustomerPage from './customer/CustomerPage';
import PartnersListPage from './partner/PartnersListPage';
import PartnerPage from './partner/PartnerPage';

const Routes = () => (
  <main>
    <Switch>
      <Route exact path='/' render={()=><HomePage /> }/>
      <Route exact path='/about' render={()=><AboutPage />}/>
      <Route exact path='/contact-us' component={ContactUsPage }/>
      <Route exact path='/customers' component={CustomersListPage }/>
      <Route exact path='/customer/:id' component={CustomerPage }/>
      <Route exact path='/partners' component={PartnersListPage }/>
      <Route exact path='/partner/:id' component={PartnerPage }/>
    </Switch>
  </main>
);

export default Routes

