/**
 * Created by ruben on 3/4/2018.
 */
import React from 'react';
import { withRouter } from 'react-router-dom';
import Header from './common/Header';
import Main from './Routes';

class App extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      this.onRouteChanged(this.props.location);
    }
  }

  onRouteChanged(location) {
    console.log(location.pathname);
  }

  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Main />
        </div>
      </div>
    );
  }
}

export default withRouter(App);
