/**
 * Created by ruben on 3/11/2018.
 */

import {PARTNER} from './_actionTypes';
import Api from '../api/Api';

export function fetchPartners() {
  return function (dispatch) {
    return Api.getPartners()
      .then(res => {
        if (res.data.success) {
          dispatch({type: PARTNER.LIST_LOAD_SUCCESS, payload: res.data.partners});
        }

      })
      .catch(err => {
        throw (err);
      });
  };
}

export function fetchSinglePartner(id) {
  return function (dispatch) {
    return Api.getPartnerById(id)
      .then(res => {
        if (res.data.success) {
          dispatch({type: PARTNER.SINGLE_LOAD_SUCCESS, payload: res.data.partner});
        }
      })
      .catch(err => {
        throw (err);
      });
  };
}
