/**
 * Created by ruben on 3/12/2018.
 */
import {CUSTOMER}  from './_actionTypes';
import Api from '../api/Api';
import axios from 'axios';

export function fetchCustomers() {
  return function (dispatch) {
    return Api.getCustomers()
      .then(res => {
        if (res.data.success) {
          dispatch({type: CUSTOMER.LIST_LOAD_SUCCESS, payload: res.data.customers});
        }

      })
      .catch(err => {
        throw (err);
      });
  };
}

export function fetchSingleCustomer(id) {
  return function (dispatch) {
    return Api.getCustomerById(id)
      .then(res => {
        if (res.data.success) {
          dispatch({type: CUSTOMER.SINGLE_LOAD_SUCCESS, payload: res.data.customer});
        }
      })
      .catch(err => {
        throw (err);
      });
  };
}

